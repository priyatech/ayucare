package com.puretech.ayucare.model;

import java.io.Serializable;

/**
 * Created by Rupesh on 14-02-2018.
 */

public class UserDTO implements Serializable {

    private String token;
    private String uid;
    private String name;
    private String company_name;
    private String phone;
    private String email;
    private String profile_photo;

    public String getTokenId() {
        return token;
    }

    public void setTokenId(String token) {
        this.token = token;
    }

    public String getUserid() {
        return uid;
    }

    public void setUserid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company_name;
    }

    public void setCompany(String company_name) {
        this.company_name = company_name;
    }

    public String getMobile() {
        return phone;
    }

    public void setMobile(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilepic() {
        return profile_photo;
    }

    public void setProfilepic(String profile_photo) {
        this.profile_photo = profile_photo;
    }

}
