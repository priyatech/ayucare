package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.puretech.ayucare.App;
import com.puretech.ayucare.adapter.ImageAdapter;
import com.puretech.ayucare.adapter.ProductAdapter;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AsynkGetProducts  extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    GetSetGo dataDTO;
    ProgressDialog pDialog = null;
    App app;
    private ArrayList<GetSetGo> dataList = new ArrayList<>();

    ProductAdapter productAdapter;
    Context context;
    RecyclerView recyclerViewBranch;
    String slot;
    // For checking login authentication
    public AsynkGetProducts( Context context , Activity act,RecyclerView recyclerViewBranch) {
        this.context = context;
        this.activity = act;
        this.recyclerViewBranch = recyclerViewBranch;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }else if (result == 401){
                //logout();
                Toast.makeText(activity, "Bad Request.", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(activity, "No data found.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String imgName, imgPath, productName, productPrice;
                    imgName = object.getString("ImageName");
                    imgPath = object.getString("ImagePath");
                    productName = object.getString("ProductName");
                    productPrice = object.getString("ProductPrice");

                    dataDTO = new GetSetGo();
                    dataDTO.setImgName(imgName);
                    dataDTO.setImgPath(imgPath);
                    dataDTO.setProductName(productName);
                    dataDTO.setProductPrice(productPrice);
                    dataList.add(dataDTO);

                }
                productAdapter = new ProductAdapter(dataList,activity);

                // this for grid view item
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                recyclerViewBranch.setLayoutManager(mLayoutManager);
                recyclerViewBranch.setItemAnimator(new DefaultItemAnimator());
                recyclerViewBranch.setAdapter(productAdapter);




            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get data.", Toast.LENGTH_LONG).show();
        }
    }

}