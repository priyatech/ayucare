package com.puretech.ayucare.activity;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.puretech.ayucare.AsynckTask.AsynkGetBranch;
import com.puretech.ayucare.R;
import com.puretech.ayucare.adapter.BranchCardAdapter;
import com.puretech.ayucare.model.GetSetGo;

import java.util.ArrayList;

public class Branch extends AppCompatActivity {

    RecyclerView recyclerViewBranch;

    TextView tv_Title;
    ImageView iv_Back;
    View view;
    AsynkGetBranch asynkGetBranch;
    ArrayList<GetSetGo> branchDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch);


        tv_Title= findViewById(R.id.tvTitle);
        tv_Title.setText("Select Branch");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);

        recyclerViewBranch = findViewById(R.id.recycler_branch);

        asynkGetBranch = new AsynkGetBranch(getApplicationContext(),Branch.this,recyclerViewBranch);
        asynkGetBranch.execute(Branch.this.getString(R.string.link) + Branch.this.getString(R.string.getBranch));

    }


}
