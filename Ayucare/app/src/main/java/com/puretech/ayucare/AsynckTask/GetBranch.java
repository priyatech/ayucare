package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.puretech.ayucare.R;
import com.puretech.ayucare.activity.BookAppointmentActivity;
import com.puretech.ayucare.activity.EditAppointment;
import com.puretech.ayucare.adapter.BranchCardAdapter;
import com.puretech.ayucare.adapter.CustomItemClickListener;
import com.puretech.ayucare.adapter.MyAdapterSpinner;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetBranch extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    GetSetGo dataDTO;
    ProgressDialog pDialog = null;
    Spinner spnBranch;
    private ArrayList<GetSetGo> dataList = new ArrayList<>();
    BranchCardAdapter branchCardAdapter;

    Context context;
    String branchId;

    // For checking login authentication
    public GetBranch( Context context , Activity act,Spinner spnBranch) {
        this.context = context;
        this.activity = act;
        this.spnBranch = spnBranch;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String BranchName = object.getString("BranchName");
                    final String BranchId = object.getString("BranchId");
                    final String City = object.getString("City");
                    final String Days = object.getString("Days");
//                    final String OnWeek = object.getString("OnWeek");

                    dataDTO = new GetSetGo();
                    dataDTO.setBranchName(BranchName);
                    dataDTO.setBranchId(BranchId);
                    branchId = dataDTO.getBranchId();
                    dataDTO.setCity(City);
                    dataDTO.setDays(Days);
//                    dataDTO.setOnWeek(OnWeek);

                    dataList.add(dataDTO);

                }


                List<String> labels = new ArrayList<String>();
                labels.add("Select Branch");
                for(int i = 0; i<dataList.size(); i++)
                {
                    labels.add(dataList.get(i).getBranchName());
                }

                ArrayAdapter<String> adp1 = new ArrayAdapter<>
                        (activity,R.layout.spinner_item,labels);
                adp1.setDropDownViewResource(R.layout.spinner_dropdown_item);
                spnBranch.setAdapter(adp1); // spn new table


            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get data.", Toast.LENGTH_LONG).show();
        }
    }


    public ArrayList<GetSetGo> getBranch()
    {
        return dataList;
    }

}
