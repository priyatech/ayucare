package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.puretech.ayucare.AsynckTask.AsynkCancelApt;
import com.puretech.ayucare.AsynckTask.AsynkGetBranch;
import com.puretech.ayucare.R;
import com.puretech.ayucare.activity.Branch;
import com.puretech.ayucare.activity.EditAppointment;
import com.puretech.ayucare.model.GetSetGo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CardHistoryAdapter extends RecyclerView.Adapter<CardHistoryAdapter.ViewHolder> {

    ArrayList<GetSetGo> list;
    Context mContext;
    Activity activity;
    CustomItemClickListener listener;
    public View mView;
    String dayOfTheWeek, day, monthString;


    public CardHistoryAdapter(ArrayList<GetSetGo> list,Activity activity, Context mContext) { // , CustomItemClickListener listener
        this.list = list;
        this.mContext = mContext;
        this.activity = activity;
//        this.listener = listener;
    }

    @NonNull
    @Override
    public CardHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_history, parent, false);
        final CardHistoryAdapter.ViewHolder mViewHolder = new CardHistoryAdapter.ViewHolder(mView);

//        mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                listener.onItemClick(v, mViewHolder.getPosition());
//
//
//            }
//        });
        return mViewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull final CardHistoryAdapter.ViewHolder holder, int position) {

        GetSetGo data = list.get(position);

        holder.txtPatient.setText(data.getPatientName());
        holder.txtPId.setText(data.getPatientAppointmentId());
        holder.txtSlots.setText(data.getAptTime());
        holder.txtBranch.setText(data.getAptBranch()+","+data.getCity());
        holder.txtStatus.setText(data.getAptstatus());

        String dt = data.getAptDate();
//        Log.d("dt -->", dt);

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = df.parse(dt);  //dateInstall
             dayOfTheWeek = (String) DateFormat.format("EEE", date1); // Thursday
             day          = (String) DateFormat.format("dd",   date1); // 20
             monthString  = (String) DateFormat.format("MMM",  date1); // Jun
        }catch (ParseException e) {
            e.printStackTrace();
        }
                holder.txtMonth.setText(monthString);
                holder.txtDate.setText(day);
                holder.txtDay.setText(dayOfTheWeek);

        holder.btnImgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bu = new Bundle();
//              System.out.println("table in PlaceOrderTabbed2 -->"+ tbl);
                bu.putString("pId",holder.txtPId.getText().toString());
                bu.putString("pName",holder.txtPatient.getText().toString());
//                bu.putString("branch",holder.txtBranch.getText().toString());
//              bu.putString("date",holder.txtPId.getText().toString());
//                bu.putString("time",holder.txtSlots.getText().toString());
                Intent intent = new Intent(mContext, EditAppointment.class);
                intent.putExtras(bu);
//                    System.out.println("afrter putSgtrin table in POlaceOrderTabbed2 -->"+ bill);
                mContext.startActivity(intent);

            }
        });

        holder.btnImgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsynkCancelApt(mContext, activity,holder.txtPId.getText().toString()).execute(mContext.getString(R.string.link) + mContext.getString(R.string.deleteApt));

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPId,txtPatient,txtSlots, txtStatus,txtBranch, txtMonth, txtDate, txtDay;
        public ImageButton btnImgEdit, btnImgCancel;

        public ViewHolder(View view) {
            super(view);
            txtPatient = view.findViewById(R.id.txt_patient);
            txtPId = view.findViewById(R.id.txt_patient_id);
            txtSlots = view.findViewById(R.id.txt_time);
            txtBranch = view.findViewById(R.id.txt_branch);
            txtStatus = view.findViewById(R.id.txt_status);

            txtMonth = view.findViewById(R.id.txt_month);
            txtDate = view.findViewById(R.id.txt_date);
            txtDay = view.findViewById(R.id.txt_day);

            btnImgEdit = view.findViewById(R.id.btn_img_edit);
            btnImgEdit.setVisibility(View.GONE);
            btnImgCancel = view.findViewById(R.id.btn_img_cancel);
        }
    }

}
