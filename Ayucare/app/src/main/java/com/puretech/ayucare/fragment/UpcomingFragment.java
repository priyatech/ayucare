package com.puretech.ayucare.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.puretech.ayucare.App;
import com.puretech.ayucare.AsynckTask.AsynkGetHistory;
import com.puretech.ayucare.R;
import com.puretech.ayucare.activity.BookAppointmentActivity;
import com.puretech.ayucare.adapter.AdapterAppointment;
import com.puretech.ayucare.model.ItemAppointment;
import com.puretech.ayucare.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressWarnings("ALL")
public class UpcomingFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    App app;
    Activity activity;
    View rootView;
    RecyclerView rcUpcoming;
    AdapterAppointment adapterAppointment;
    List<ItemAppointment> appointmentList = new ArrayList<>();
    long totalSize = 0;
    Boolean isPermitted = true;
    String message, alert;
    SwipeRefreshLayout swipeRefreshLayout;
    String lang;
    String mob;
    private SharedPreferences sharedPreferences;
    //Button btn_Appointment;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        app = (App) getActivity().getApplication();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_upcoming, container, false);

        rcUpcoming = rootView.findViewById(R.id.recycler_upcoming_appt);

        sharedPreferences = activity.getSharedPreferences("Reg", Context.MODE_PRIVATE);
        mob = sharedPreferences.getString("mob", "0");
        Log.d("frag mob -->", mob);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //checkConnection();
                                    }
                                }
        );

        new AsynkGetHistory(getContext(),activity, rcUpcoming).execute(app.getString(R.string.link) + app.getString(R.string.getPatientUpcomingHistory)+"/"+mob);
        checkConnection();
        return rootView;
    }

    @Override
    public void onRefresh() {
        checkConnection();
    }

    public void checkConnection() {
        if (CommonUtil.isNetworkAvailable(activity)) {
            swipeRefreshLayout.setRefreshing(false);
            //checkRunTimePermissionPic();
        } else {
            app.showSnackBar(activity, "Please check network connectivity");
            //setAdapter();
        }
    }

}
