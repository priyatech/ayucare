package com.puretech.ayucare.adapter;

import android.content.Context;
import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.GetSetGo;
import com.puretech.ayucare.util.ImageUtil;

import java.text.BreakIterator;
import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    ArrayList<GetSetGo> expenseList;
    Context mContext;
    public View mView;

    public ImageAdapter(ArrayList<GetSetGo> expenseList, Context mContext) {
        this.expenseList = expenseList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_image, parent, false);
        final ImageAdapter.ViewHolder mViewHolder = new ImageAdapter.ViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GetSetGo data = expenseList.get(position);
        holder.txtName.setText(data.getImgName());

        String imgPath = "http://ayucareapp.ayucareclinic.in/"+data.getImgPath();
        Log.d("imgPath-->", imgPath);
        ImageUtil.displayImage(holder.img, imgPath, null);
    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;
        public ImageView img;

        public ViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.txt_title);
            img = view.findViewById(R.id.img_thumbnail);
        }
    }

}
