package com.puretech.ayucare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Di9esh MisaRs on 10/02/2018.
 */

public class CustomAdapter extends BaseAdapter {

    Context context;
    String DlItemName[];
    int DlItemImage[];
    LayoutInflater layoutInflater;
    List list = new ArrayList();
    public CustomAdapter(Context aapplicationdsContext, String[]DlItemName, int[]DlItemImage){

        this.context=context;
        this.DlItemName=DlItemName;
        this.DlItemImage=DlItemImage;
        layoutInflater=(LayoutInflater.from(aapplicationdsContext));
    }
    @Override
    public int getCount() {return DlItemName.length;}

    @Override
    public Object getItem(int i) {
        return null;}

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view=layoutInflater.inflate(R.layout.cardlist,null);

        TextView textView=(TextView)view.findViewById(R.id.textg);
        ImageView imageView=(ImageView)view.findViewById(R.id.imageg);
        textView.setText(DlItemName[i]);
        imageView.setImageResource(DlItemImage[i]);
        return  view;


    }

}
