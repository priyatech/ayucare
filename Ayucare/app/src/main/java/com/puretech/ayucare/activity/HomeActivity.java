package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.adapter.CustomAdapter;

public class HomeActivity extends AppCompatActivity {

    Activity activity;
    GridView gv_View;
    Intent about, services, gallery, blog, testimonials, availability, reach, contact, myAppointment, iBookAppointment, branch;
    Button b_BookAppointment;
    String DlItemName[]={"About us","Products","Gallery","Doc Blog","Testimonials","Availability","Reach us","Contact us","Appointment"};
    int DlItemImage[]={R.mipmap.abt_us,R.mipmap.products,R.mipmap.gallery,R.mipmap.doc_blog,R.mipmap.testimonals,R.mipmap.availability,R.mipmap.reach_us,R.mipmap.contact_us,R.mipmap.appointments};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        activity=this;
        gv_View = findViewById(R.id.gv_View);
        b_BookAppointment= findViewById(R.id.btn_Appointment);
        about = new Intent(activity, AboutusActivity.class);
        services = new Intent(activity, ProductActivity.class);
        gallery = new Intent(activity, GalleryActivity.class);
        blog = new Intent(activity, BlogActivity.class);
        testimonials = new Intent(activity, TestimonialActivity.class);
        availability = new Intent(activity, AvailabilityActivity.class);
        reach = new Intent(activity, ReachUsActivity.class);
        contact = new Intent(activity, ContactUsActivity.class);
        myAppointment = new Intent(activity, MyAppointmentActivity.class);
        iBookAppointment = new Intent(activity, BookAppointmentActivity.class);
        branch = new Intent(activity, BookAppointmentActivity.class);
        CustomAdapter customeAdapter=new CustomAdapter(getApplicationContext(), DlItemName, DlItemImage);
        gv_View.setAdapter(customeAdapter);

        gv_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:startActivity(about);
                        break;
                    case 1:startActivity(services);
                        break;
                    case 2:startActivity(gallery);
                        break;
                    case 3:startActivity(blog);
                        break;
                    case 4:startActivity(testimonials);
                        break;
                    case 5:startActivity(availability);
                        break;
                    case 6:startActivity(reach);
                        break;
                    case 7:startActivity(contact);
                        break;
                    case 8:startActivity(myAppointment);
                        break;
                }
            }
        });
        b_BookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,Branch.class));
            }
        });
    }
}
