package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.ItemAppointment;

import java.util.List;

/**
 * Created by Rupesh on 18-05-2018.
 */

public class AdapterAppointment extends BaseAdapter {
    private List<ItemAppointment> itemAppointments;
    private Activity activity;
    private LayoutInflater layoutInflater;

    public AdapterAppointment(List<ItemAppointment> itemAppointments, Activity activity) {
        this.itemAppointments = itemAppointments;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemAppointments.size();
    }

    @Override
    public ItemAppointment getItem(int position) {
        return itemAppointments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AdapterAppointment.ViewHolder viewHolder;
        final ItemAppointment itemAppointments = getItem(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_appointment, parent, false); //item_friends
            viewHolder = new AdapterAppointment.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdapterAppointment.ViewHolder) convertView.getTag();
        }

        viewHolder.tvDate.setText(itemAppointments.getDate());
        viewHolder.tvMonth.setText(itemAppointments.getMonth());
        viewHolder.tvName.setText(itemAppointments.getName());
        viewHolder.tvTime.setText(itemAppointments.getTime());

        return convertView;
    }

    private class ViewHolder {

        private TextView tvName, tvDate, tvMonth, tvTime;

        private ViewHolder(View view) {
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvMonth = (TextView) view.findViewById(R.id.tvMonth);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
        }
    }

}