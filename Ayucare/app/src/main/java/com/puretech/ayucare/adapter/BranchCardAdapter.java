package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.GetSetGo;

import java.util.ArrayList;

public class BranchCardAdapter extends RecyclerView.Adapter<BranchCardAdapter.ViewHolder> {
    ArrayList<GetSetGo> expenseList;

    Context mContext;
    CustomItemClickListener listener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_get_branch_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GetSetGo data = expenseList.get(position);

        String branch = data.getBranchName();
        String city = data.getCity();
        holder.txtBranch.setText(branch+","+city);
        holder.txtDays.setText(data.getDays());
    }


    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public BranchCardAdapter(Context mContext, ArrayList<GetSetGo> data, CustomItemClickListener listener) {
        this.expenseList = data;
        this.mContext = mContext;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtBranch,txtDays;

        ViewHolder(View view) {
            super(view);
            txtBranch = view.findViewById(R.id.txt_branch_city);
            txtDays = view.findViewById(R.id.txt_days);
        }
    }
}