package com.puretech.ayucare.activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;

public class AboutusActivity extends AppCompatActivity {

    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        activity=this;
        tv_Title=findViewById(R.id.tvTitle);
        tv_Title.setText("About Us");
        iv_Back = findViewById(R.id.ivBack);
        
        // hey this is test for pull
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
