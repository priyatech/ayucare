package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import com.puretech.ayucare.activity.MyAppointmentActivity;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AsynkUpdateApt extends AsyncTask<String, String, String> {

    String AppointmentDate,AppointmentTime,PatientName,patientAppointmentId,BranchId;
    String t_id,w_id,status= null;
    Activity activity;
    String order_id,date;

    public AsynkUpdateApt(Activity activity, String AppointmentDate, String AppointmentTime, String PatientName , String patientAppointmentId, String BranchId) {
        this.AppointmentDate = AppointmentDate;
        this.activity = activity;
        this.AppointmentTime = AppointmentTime;
        this.PatientName = PatientName;
        this.patientAppointmentId = patientAppointmentId;
        this.BranchId = BranchId;
    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("AppointmentDate",AppointmentDate);
            jsonObject.put("AppointmentTime",AppointmentTime);
            jsonObject.put("PatientName",PatientName);
            jsonObject.put("patientAppointmentId",patientAppointmentId);
            jsonObject.put("BranchId",BranchId);

            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        Intent intent = new Intent(activity, MyAppointmentActivity.class);
        activity.startActivity(intent);
        if(result!=null){
            Toast.makeText(activity,"Appointment Updated Successfully.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Doesn't updated.",Toast.LENGTH_LONG).show();
        }
    }
}
