package com.puretech.ayucare.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.GetSetGo;
import com.puretech.ayucare.util.ImageUtil;

import java.util.ArrayList;

public class ProductAdapter  extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    ArrayList<GetSetGo> expenseList;
    Context mContext;
    public View mView;

    public ProductAdapter(ArrayList<GetSetGo> expenseList, Context mContext) {
        this.expenseList = expenseList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false);
        final ProductAdapter.ViewHolder mViewHolder = new ProductAdapter.ViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.ViewHolder holder, int position) {

        GetSetGo data = expenseList.get(position);
        holder.txtName.setText(data.getProductName());
        holder.txtProductPrice.setText(data.getProductPrice()+" /-");

//        String imgPath = "http://ayucareapp.ayucareclinic.in/"+data.getImgPath();
//        Log.d("imgPath-->", imgPath);
//        ImageUtil.displayImage(holder.img, imgPath, null);

        ImageUtil.displayImage(holder.img, data.getImgPath(), null);
    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName,  txtDesc;
        TextView txtProductPrice;
        public ImageView img;

        public ViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.tvProdName);
            txtDesc = view.findViewById(R.id.tvDesc);
            txtProductPrice = view.findViewById(R.id.txt_price);
            img = view.findViewById(R.id.ivImage);
        }
    }

}
