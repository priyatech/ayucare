package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.puretech.ayucare.App;
import com.puretech.ayucare.R;
import com.puretech.ayucare.model.ItemCategory;
import com.puretech.ayucare.util.ImageUtil;

import java.util.List;


/**
 * Created by Rupesh on 22-02-2018.
 */

public class AdapterImage extends BaseAdapter {
    private List<ItemCategory> itemCategories;
    private Activity activity;
    private LayoutInflater layoutInflater;
    private App app;

    public AdapterImage(List<ItemCategory> itemCategories, Activity activity) {
        this.itemCategories = itemCategories;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        app = (App) activity.getApplication();
    }

    @Override
    public int getCount() {
        return itemCategories.size();
    }

    @Override
    public ItemCategory getItem(int position) {
        return itemCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AdapterImage.ViewHolder viewHolder;
        final ItemCategory itemCategory = getItem(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.image_list, parent, false); //item_friends
            viewHolder = new AdapterImage.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdapterImage.ViewHolder) convertView.getTag();
        }

        viewHolder.rlDescription.setVisibility(View.GONE);
        ImageUtil.displayImage(viewHolder.ivImage, itemCategory.getImage(), null);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent detail = new Intent(activity, ImageDetailViewActivity.class);
//                detail.putExtra("id", itemCategory.getId());
//                detail.putExtra("image", itemCategory.getImage());
//                activity.startActivity(detail);
            }
        });

        return convertView;
    }

    private class ViewHolder {

        private ImageView ivImage;
        private TextView tvTitle, tvIssued;
        private RelativeLayout rlDescription;

        public ViewHolder(View view) {
            ivImage = view.findViewById(R.id.ivImage);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvIssued = view.findViewById(R.id.tvIssued);
            rlDescription = view.findViewById(R.id.rlDescription);
        }
    }

}
