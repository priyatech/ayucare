package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.puretech.ayucare.R;
import com.puretech.ayucare.activity.BookAppointmentActivity;
import com.puretech.ayucare.adapter.BranchCardAdapter;
import com.puretech.ayucare.adapter.CustomItemClickListener;
import com.puretech.ayucare.adapter.MyAdapterSpinner;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.puretech.ayucare.App.TAG;

public class AsynkGetBranch extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    GetSetGo dataDTO;
    ProgressDialog pDialog = null;
    MyAdapterSpinner spnBranch;
    private ArrayList<GetSetGo> dataList = new ArrayList<>();
    BranchCardAdapter branchCardAdapter;

    Context context;
    RecyclerView recyclerViewBranch;
    String branchId;
    Spinner spnbranch;

    // For checking login authentication
    public AsynkGetBranch( Context context , Activity act,RecyclerView recyclerViewBranch) {
        this.context = context;
        this.activity = act;
        this.recyclerViewBranch = recyclerViewBranch;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String BranchName = object.getString("BranchName");
                    final String BranchId = object.getString("BranchId");
                    final String City = object.getString("City");
                    final String Days = object.getString("Days");
//                    final String OnWeek = object.getString("OnWeek");

                    dataDTO = new GetSetGo();
                    dataDTO.setBranchName(BranchName);
                    dataDTO.setBranchId(BranchId);
                    branchId = dataDTO.getBranchId();
                    dataDTO.setCity(City);
                    dataDTO.setDays(Days);
//                    dataDTO.setOnWeek(OnWeek);

                    dataList.add(dataDTO);

                }

                    branchCardAdapter = new BranchCardAdapter(activity, dataList, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {

                            Bundle bu = new Bundle();

                            String bId = dataList.get(position).getBranchId();
                            bu.putString("branchId",bId);

                            Intent intent = new Intent(activity, BookAppointmentActivity.class);
                            intent.putExtras(bu);
                            activity.startActivity(intent);
                            activity.finish();
//                            Log.d(TAG, "clicked position:" + position);
                        }
                    });

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                    recyclerViewBranch.setLayoutManager(mLayoutManager);
                    recyclerViewBranch.setItemAnimator(new DefaultItemAnimator());
                    recyclerViewBranch.setAdapter(branchCardAdapter);


            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get data.", Toast.LENGTH_LONG).show();
        }
    }


    public ArrayList<GetSetGo> getBranch()
    {
        return dataList;
    }

   }



