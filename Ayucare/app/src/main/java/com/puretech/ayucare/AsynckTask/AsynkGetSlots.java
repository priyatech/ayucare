package com.puretech.ayucare.AsynckTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.puretech.ayucare.adapter.SlotCardAdapter;
import com.puretech.ayucare.model.GetSetGo;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
public class AsynkGetSlots extends AsyncTask<String, String, String> {

    String status = null;
    @SuppressLint("StaticFieldLeak")
    Activity activity;
    private ProgressDialog pDialog = null;
    private ArrayList<GetSetGo> dataList = new ArrayList<>();

    private SlotCardAdapter slotCardAdapter;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    @SuppressLint("StaticFieldLeak")
    private RecyclerView recyclerViewBranch;

    // For checking login authentication
    public AsynkGetSlots( Context context , Activity act,RecyclerView recyclerViewBranch) {
        this.context = context;
        this.activity = act;
        this.recyclerViewBranch = recyclerViewBranch;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
//                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String slot = object.getString("AptmentSlot");

                    GetSetGo dataDTO = new GetSetGo();
                    dataDTO.setSlot(slot);
                    dataList.add(dataDTO);

                }

//                slotCardAdapter = new SlotCardAdapter(activity, dataList, new CustomItemClickListener() {
//                    @Override
//                    public void onItemClick(View v, int position) {
////                        slot  = dataList.get(position).getSlot();
//                        String pat = dataList.get(position).getPatientName();
//
//                      // SlotCardAdapter.ViewHolder mViewHolder = new SlotCardAdapter.ViewHolder(v);// not getting view thats whhy  not setting color
//
////                        row_index=position;
////                            slotCardAdapter.notifyDataSetChanged();
////
////                            if(row_index==position)
////                            {
////                                mViewHolder.crdTime.setBackgroundColor(Color.parseColor("#e8e20d1f"));
////                                mViewHolder.txtSlots.setTextColor(Color.parseColor("#ffffff"));
////                            }
////                            else
////                            {
////                                mViewHolder.crdTime.setBackgroundColor(Color.parseColor("#ffffff"));
////                                mViewHolder.txtSlots.setTextColor(Color.parseColor("#000000"));
////                            }
//
//
//
//                                if(pat.equals("")){
//                                    slot  = dataList.get(position).getSlot();
//                                    Toast.makeText(activity, "You have selected "+ slot +" for appointment", Toast.LENGTH_SHORT).show();
//                                }else{
//                                    Toast.makeText(activity, "Regret!! This Slot is Not available", Toast.LENGTH_SHORT).show();
//                                }
//
////
////
////                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm a");
////                        Calendar calendar = Calendar.getInstance();
////                        currTime = simpleDateFormat.format(calendar.getTime());
////
////
////                        Log.d("currTime-->", currTime);
////                        try {
////                            date1 = simpleDateFormat.parse(currTime);
////                            Log.d("currTime-->", currTime);
////                        } catch (ParseException e) {
////                            e.printStackTrace();
////                        }
////                        try {
////                            date2 = simpleDateFormat.parse(slot);
////                        } catch (ParseException e) {
////                            e.printStackTrace();
////                        }
//////
////                        long difference = date2.getTime() - date1.getTime();
////                        days = (int) (difference / (1000*60*60*24));
////                        hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
////                        min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
////                        hours = (hours < 0 ? -hours : hours);
//
//                    }
//
//
//                });

                slotCardAdapter = new SlotCardAdapter(activity,dataList);
// this for grid view item
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, 3);
                recyclerViewBranch.setLayoutManager(mLayoutManager);
                recyclerViewBranch.setItemAnimator(new DefaultItemAnimator());
                recyclerViewBranch.setAdapter(slotCardAdapter);

            } catch (NullPointerException ignored){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get data.", Toast.LENGTH_LONG).show();
        }
    }


    public String mySlot(){
        //    String patient;
        String slot = slotCardAdapter.slot();
            return slot;
    }


}


