package com.puretech.ayucare.activity;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.puretech.ayucare.App;
import com.puretech.ayucare.R;
import com.puretech.ayucare.adapter.AdapterCategory;
import com.puretech.ayucare.model.ItemCategory;
import com.puretech.ayucare.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

public class BlogActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    App app;
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    ListView lvBlogs;
    AdapterCategory adapterCategory;
    List<ItemCategory> NewsList = new ArrayList<>();
    long totalSize = 0;
    Boolean isPermitted = true;
    String message, alert;
    SwipeRefreshLayout swipeRefreshLayout;
    String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        app = (App) getApplication();
        activity=this;
        tv_Title= findViewById(R.id.tvTitle);
        tv_Title.setText("Doc Blog");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lvBlogs = findViewById(R.id.lvBlogs);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //checkConnection();
                                    }
                                }
        );

        checkConnection();

    }

    @Override
    public void onRefresh() {
        checkConnection();
    }

    public void checkConnection() {
        if (CommonUtil.isNetworkAvailable(activity)) {
            swipeRefreshLayout.setRefreshing(false);
            //checkRunTimePermissionPic();
        } else {
            app.showSnackBar(activity, "Please check network conectivity");
            //setAdapter();
        }
    }

}
