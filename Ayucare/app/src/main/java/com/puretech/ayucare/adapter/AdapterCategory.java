package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.ItemCategory;
import com.puretech.ayucare.util.ImageUtil;

import java.util.List;


/**
 * Created by Rupesh on 21-02-2018.
 */

public class AdapterCategory extends BaseAdapter {
    private List<ItemCategory> itemCategories;
    private Activity activity;
    private LayoutInflater layoutInflater;

    public AdapterCategory(List<ItemCategory> itemCategories, Activity activity) {
        this.itemCategories = itemCategories;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemCategories.size();
    }

    @Override
    public ItemCategory getItem(int position) {
        return itemCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AdapterCategory.ViewHolder viewHolder;
        final ItemCategory itemCategory = getItem(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_category, parent, false); //item_friends
            viewHolder = new AdapterCategory.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdapterCategory.ViewHolder) convertView.getTag();
        }

        //String str = itemCategory.getImage();
        ImageUtil.displayImage(viewHolder.ivImage, itemCategory.getImage(), null);
        viewHolder.tvProdName.setText(itemCategory.getTitle());
        viewHolder.tvDate.setText(itemCategory.getCreatedAt());
        viewHolder.tvDesc.setText(itemCategory.getDescription());
        viewHolder.tvDesc.setVisibility(View.GONE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //app.getPreferences().setPageId(itemCategory.getTitle());
//                Intent detail = new Intent(activity, DetailViewActivity.class);
//                detail.putExtra("id", itemCategory.getId());
//                detail.putExtra("image", itemCategory.getImage());
//                detail.putExtra("title", itemCategory.getTitle());
//                detail.putExtra("description", itemCategory.getDescription());
//                detail.putExtra("page","other");
//                detail.putExtra("reference_url",itemCategory.getReferenceUrl());
//                activity.startActivity(detail);

            }
        });

        return convertView;
    }

    private class ViewHolder {

        private ImageView ivImage;
        private TextView tvProdName, tvDate, tvDesc;

        private ViewHolder(View view) {
            ivImage = (ImageView) view.findViewById(R.id.ivImage);
            tvProdName = (TextView) view.findViewById(R.id.tvProdName);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
        }
    }

}
