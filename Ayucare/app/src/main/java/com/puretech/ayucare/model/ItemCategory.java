package com.puretech.ayucare.model;

import java.io.Serializable;

/**
 * Created by Rupesh on 21-02-2018.
 */

public class ItemCategory implements Serializable {

    private String id;
    private String title;
    private String short_description;
    private String description;
    private String image;
    private String reference_url;
    private String created_at;
    private String updated_at;
    private String issue_year;
    private String file_path;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return short_description;
    }

    public void setShortDescription(String short_description) {
        this.short_description = short_description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReferenceUrl(){
        return reference_url;
    }

    public void setReferenceUrl(String reference_url){
        this.reference_url = reference_url;
    }

    public String getCreatedAt(){
        return created_at;
    }

    public void setCreatedAt(String created_at){
        this.created_at = created_at;
    }

    public String getUpdatedAt(){
        return updated_at;
    }

    public void setUpdatedAt(String updated_at){
        this.updated_at = updated_at;
    }

    public String getIssuedYear(){
        return issue_year;
    }

    public void setIssuedYear(String issue_year){
        this.issue_year = issue_year;
    }

    public String getFilePath(){
        return file_path;
    }

    public void setFilePath(String file_path){
        this.file_path = file_path;
    }

}
