package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.puretech.ayucare.activity.MyAppointmentActivity;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsynkCancelApt extends AsyncTask<String, String, String> {
    String status= null;
    Activity activity;
    Context context;

    String patientAppointmentId;

    public AsynkCancelApt(Context context,  Activity activity, String patientAppointmentId) {
        this.context = context;
        this.activity = activity;
        this.patientAppointmentId = patientAppointmentId;
       }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("patientAppointmentId",patientAppointmentId);  //object name is case sensEtive. it must be same as service parameter.
            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);
//        Log.d("Result deleted-->>", result);
        activity.startActivity(new Intent(activity, MyAppointmentActivity.class));
        activity.finish();
        if(result!=null){
            Toast.makeText(activity,"Appointment Cancelled Successfuly.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Doesn't cancel Order.",Toast.LENGTH_LONG).show();
        }
    }

}