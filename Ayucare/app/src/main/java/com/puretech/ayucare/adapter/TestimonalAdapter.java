package com.puretech.ayucare.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.GetSetGo;
import com.puretech.ayucare.util.ImageUtil;
import com.puretech.ayucare.view.RoundedImageView;

import java.util.ArrayList;

public class TestimonalAdapter extends RecyclerView.Adapter<TestimonalAdapter.ViewHolder> {

    ArrayList<GetSetGo> expenseList;
    Context mContext;
    public View mView;

    public TestimonalAdapter(ArrayList<GetSetGo> expenseList, Context mContext) {
        this.expenseList = expenseList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public TestimonalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_testimonials, parent, false);
        final TestimonalAdapter.ViewHolder mViewHolder = new TestimonalAdapter.ViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TestimonalAdapter.ViewHolder holder, int position) {

        GetSetGo data = expenseList.get(position);
        holder.txtName.setText(" - "+data.getPatientName());
        holder.txtDesc.setText(data.getFeedback());

//        String imgPath = "http://ayucareapp.ayucareclinic.in/"+data.getImgPath();
//        Log.d("imgPath-->", imgPath);
//        ImageUtil.displayImage(holder.img, imgPath, null);

        ImageUtil.displayImage(holder.rImg, data.getImgPath(), null);
    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName,  txtDesc;
        RoundedImageView rImg;

        public ViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.tvCreator);
            txtDesc = view.findViewById(R.id.tvDescription);
            rImg = view.findViewById(R.id.ivCreator);
        }
    }

}
