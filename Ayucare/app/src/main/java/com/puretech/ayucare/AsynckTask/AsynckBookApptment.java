package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.puretech.ayucare.activity.Branch;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AsynckBookApptment  extends AsyncTask<String, String, String> {

    String result;


    String apptDate, apptTime, patient, mobile, branchId,status= null;
    Activity activity;

    public AsynckBookApptment(Activity activity,String apptDate,String apptTime,String patient,String mobile,String branchId) { //, ArrayList<GetandSet> itemlist
        this.activity = activity;
        this.apptDate = apptDate; // same date
        this.apptTime  = apptTime;
        this.patient = patient;
        this.mobile = mobile;
        this.branchId = branchId;
    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("AppointmentDate",apptDate);  //object name is case sensEtive. it must be same as service parameter.
            jsonObject.put("AppointmentTime",apptTime);
            jsonObject.put("PatientName",patient);
            jsonObject.put("MobileNo",mobile);
            jsonObject.put("BranchId",branchId);
            //jsonObject.put("order_id",order_id);


            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);

        Log.d("result----->",result);
        if(result=="0"){
            Toast.makeText(activity,"This Slot is Booked already.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Appointment Booked, Id is "+result,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(activity, Branch.class);
            activity.startActivity(intent);
            activity.finish();
        }
    }





}