package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.fragment.PastFragment;
import com.puretech.ayucare.fragment.UpcomingFragment;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

@SuppressWarnings("ALL")

public class MyAppointmentActivity extends AppCompatActivity {
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    View view;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    Button btn_Appointment;
    GifImageView gif;
    RelativeLayout rlBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_appointment);
        activity=this;
        tv_Title=(TextView)findViewById(R.id.tvTitle);
        tv_Title.setText("My Appointments");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        gif = findViewById(R.id.gifView);
        rlBtn = findViewById(R.id.footer);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setupTabText();
                viewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        tabLayout.getTabAt(0);
                        break;
                    case 1:
                        tabLayout.getTabAt(1);
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                super.onTabUnselected(tab);
                setupTabText();
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //super.onTabReselected(tab);
            }
        });

    }

    private void setupTabText(){
        tabLayout.setTabTextColors(Color.parseColor("#9d9d9d"), Color.parseColor("#246309"));
    }

    //fragments to the view pagers with proper name
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new UpcomingFragment(), "Upcoming");
        adapter.addFragment(new PastFragment(), "Past");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
