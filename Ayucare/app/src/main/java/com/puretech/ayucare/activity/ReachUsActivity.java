package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.puretech.ayucare.R;

@SuppressWarnings("ALL")

public class ReachUsActivity extends AppCompatActivity {
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    private GoogleMap googleMap;
    Button btn_Appointment, btn_Appointment1 , btn_Appointment2;
    FloatingActionButton fab, fab1, fab2;
    double latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reach_us);
        activity=this;
        tv_Title=(TextView)findViewById(R.id.tvTitle);
        tv_Title.setText("Reach Us");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fab = findViewById(R.id.fab);
        fab1 = findViewById(R.id.fab1);
        fab2 = findViewById(R.id.fab2);
        btn_Appointment = findViewById(R.id.btn_Appointment);
        btn_Appointment1 = findViewById(R.id.btn_Appointment1);
        btn_Appointment2 = findViewById(R.id.btn_Appointment2);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "https://www.google.co.in/maps/place/Ayu+Care+Clinic/@18.5410908,73.7904275,17z/data=!4m12!1m6!3m5!1s0x3bc2bee22f6c76cd:0x229aa1f916528f4!2sAyu+Care+Clinic!8m2!3d18.5410857!4d73.7926162!3m4!1s0x3bc2bee22f6c76cd:0x229aa1f916528f4!8m2!3d18.5410857!4d73.7926162";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "https://www.google.co.in/maps/place/Ayu+Care+Clinic/@18.5410908,73.7904275,17z/data=!4m12!1m6!3m5!1s0x3bc2bee22f6c76cd:0x229aa1f916528f4!2sAyu+Care+Clinic!8m2!3d18.5410857!4d73.7926162!3m4!1s0x3bc2bee22f6c76cd:0x229aa1f916528f4!8m2!3d18.5410857!4d73.7926162";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "https://www.google.co.in/maps/place/Ayu+Care+Clinic/@18.5410908,73.7904275,17z/data=!4m12!1m6!3m5!1s0x3bc2bee22f6c76cd:0x229aa1f916528f4!2sAyu+Care+Clinic!8m2!3d18.5410857!4d73.7926162!3m4!1s0x3bc2bee22f6c76cd:0x229aa1f916528f4!8m2!3d18.5410857!4d73.7926162";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        btn_Appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(activity, Branch.class);
                startActivity(i);
            }
        });

        btn_Appointment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(activity, Branch.class);
                startActivity(i);
            }
        });


        btn_Appointment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(activity, Branch.class);
                startActivity(i);
            }
        });


        try {
            // Loading map
            latitude = 18.5410857;
            longitude = 73.7926162;
            initilizeMap(latitude,longitude);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap(double latitude,double longitude) {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }else{
                // latitude and longitude
//                double latitude = 18.5410857;
//                double longitude = 73.7926162;
                float zoom = googleMap.getCameraPosition().zoom;


                // create marker
                MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Ayu Care");

                // Changing marker icon
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.heart));

                // adding marker
                googleMap.addMarker(marker);
                //googleMap.animateCamera( CameraUpdateFactory.zoomTo( 15.0f ) );
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude),15.0f));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap(latitude,longitude);
    }

}
