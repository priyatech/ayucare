package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import com.puretech.ayucare.activity.LoginActivity;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AsynkRegister extends AsyncTask<String, String, String> {

    String result;
    Activity activity;

    String name, today, mobile, password, status= null;

    public AsynkRegister(Activity activity, String name, String today, String mobile, String password) {
        this.activity = activity;
        this.name = name;
        this.today = today;
        this.mobile = mobile;
        this.password = password;
    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("patientName",name);  //object name is case sensEtive. it must be same as service parameter.
            jsonObject.put("registrationDate",today);
            jsonObject.put("mobileNo",mobile);
            jsonObject.put("passWord",password);
            jsonObject.put("status","Activate");
            //jsonObject.put("order_id",order_id);

            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
//                StringBuilder sb = new StringBuilder();
                String line;
                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);

        if(result!=null){
            Toast.makeText(activity,"Saved Successfuly.",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(activity, LoginActivity.class);
                    activity.startActivity(intent);
        }else{
            Toast.makeText(activity,"Doesn't Saved Data.",Toast.LENGTH_LONG).show();
        }
    }
    public String orderid(){return result;}
}