package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.puretech.ayucare.adapter.CardHistoryAdapter;
import com.puretech.ayucare.adapter.CustomItemClickListener;
import com.puretech.ayucare.adapter.SlotCardAdapter;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AsynkGetHistory extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    GetSetGo dataDTO;
    ProgressDialog pDialog = null;
    private ArrayList<GetSetGo> dataList = new ArrayList<>();
    private ArrayList<GetSetGo> pastlist = new ArrayList<>();
    private ArrayList<GetSetGo> upcoming = new ArrayList<>();

    CardHistoryAdapter cardHistoryAdapter;
    Context context;
    RecyclerView recyclerViewPast;
    String aptDate, pass;

    // For checking login authentication
    public AsynkGetHistory( Context context , Activity act,RecyclerView recyclerViewPast) {
        this.context = context;
        this.activity = act;
        this.recyclerViewPast = recyclerViewPast;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data..");
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String patientAppointmentId,Patient, branch, date, status, time, city;
                    patientAppointmentId = object.getString("PatientAppointmentId");
                    Patient = object.getString("Patient");
                    branch = object.getString("AptmentBrand");
                    date = object.getString("AptmentDate");
                    status = object.getString("AptmentSatus");
                    time = object.getString("AptmentTime");
                    city = object.getString("City");

                    dataDTO = new GetSetGo();

                    dataDTO.setPatientAppointmentId(patientAppointmentId);
                    dataDTO.setPatientName(Patient);
                    dataDTO.setAptBranch(branch);
                    dataDTO.setAptDate(date);
                    aptDate = dataDTO.getAptDate();
                    dataDTO.setAptstatus(status);
                    dataDTO.setAptTime(time);
                    dataDTO.setCity(city);

                    dataList.add(dataDTO);

                }

                cardHistoryAdapter = new CardHistoryAdapter(dataList,activity, context);
                // , new CustomItemClickListener() {
//                @Override
//                public void onItemClick(View v, int position) {
//
//                }
//            }

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                recyclerViewPast.setLayoutManager(mLayoutManager);
                recyclerViewPast.setItemAnimator(new DefaultItemAnimator());
                recyclerViewPast.setAdapter(cardHistoryAdapter);


            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get data.", Toast.LENGTH_LONG).show();
        }
    }
}


