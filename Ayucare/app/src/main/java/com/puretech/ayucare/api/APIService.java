package com.puretech.ayucare.api;

import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * Created by rupesh on 16-02-2018.
 */

public interface APIService {

    //Check App version
    @GET("tenant/version")
    Call<Map<String, Object>> getAppVersion(@Query("version") String nameVersion);

    //Registration
    @POST("register")
    Call<Map<String, Object>> userRegister(@Body RequestBody requestBody);

    @POST("authUser")
    Call<Map<String, Object>> verifyOTP(@Body RequestBody requestBody);

    @GET("otp")
    Call<Map<String, Object>> getOTP(@Query("phone") String mobile);

    //Get user's profile after logged in successfully
    @GET("profile")
    Call<Map<String, Object>> getProfile();

    //Profile update
    @PUT("profile")
    Call<Map<String, Object>> updateProfile(@Body RequestBody requestBody);

    @PUT("profile/password")
    Call<Map<String, Object>> setNewPassword(@Body RequestBody requestBody);

    @GET("category")
    Call<Map<String, Object>> getCategory();

    @POST("products")
    Call<Map<String, Object>> getProduct(@Body RequestBody requestBody);

    @POST("imIntrested")
    Call<Map<String, Object>> imInterested(@Body RequestBody requestBody);

    @GET("newsEvents")
    Call<Map<String, Object>> getNewsEvents();

    @GET("services")
    Call<Map<String, Object>> getServices();

    @GET("industries")
    Call<Map<String, Object>> getIndustries();

    @GET("resourcesCategory")
    Call<Map<String, Object>> getResourcesCategory();

    @POST("resources")
    Call<Map<String, Object>> getResources(@Body RequestBody requestBody);

    @GET("https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCcwNFs2BMNQ4sHOU28oa1jQ&maxResults=25&key=AIzaSyB4usZOv7RCFeyFGiW_WQyEE2wPpE_OUGM")
    Call<Map<String, Object>> getVideos();

    @GET("imageGallery")
    Call<Map<String, Object>> getImages();

    @GET("empower")
    Call<Map<String, Object>> getEmpower();

    @GET("career")
    Call<Map<String, Object>> getCareer();

    @POST("career")
    Call<Map<String, Object>> jobApply(@Body RequestBody requestBody);

    @GET("contact")
    Call<Map<String, Object>> getContacts();

    @POST("enquiry")
    Call<Map<String, Object>> makeEnquiry(@Body RequestBody requestBody);

}
