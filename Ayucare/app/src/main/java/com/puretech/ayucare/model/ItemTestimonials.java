package com.puretech.ayucare.model;

import java.io.Serializable;

/**
 * Created by Rupesh on 18-05-2018.
 */

public class ItemTestimonials implements Serializable {

    private String id;
    private String title;
    private String description;
    private String image;
    private String created_by;
    private String created_at;
    private String updated_at;
    private String issue_year;
    private String file_path;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedBy(){
        return created_by;
    }

    public void setCreatedBy(String created_by){
        this.created_by = created_by;
    }

    public String getCreatedAt(){
        return created_at;
    }

    public void setCreatedAt(String created_at){
        this.created_at = created_at;
    }

    public String getUpdatedAt(){
        return updated_at;
    }

    public void setUpdatedAt(String updated_at){
        this.updated_at = updated_at;
    }

    public String getIssuedYear(){
        return issue_year;
    }

    public void setIssuedYear(String issue_year){
        this.issue_year = issue_year;
    }

    public String getFilePath(){
        return file_path;
    }

    public void setFilePath(String file_path){
        this.file_path = file_path;
    }

}

