package com.puretech.ayucare.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.GetSetGo;

import java.util.ArrayList;

public class SlotCardAdapter extends RecyclerView.Adapter<SlotCardAdapter.ViewHolder>
{
    private ArrayList<GetSetGo> expenseList;

    //    CustomItemClickListener listener;
private int row_index=0;
    public String time;

    public SlotCardAdapter(Context mContext, ArrayList<GetSetGo> data) { //, CustomItemClickListener listener
        this.expenseList = data;
        //        this.listener = listener;
    }


    @NonNull
    @Override
    public SlotCardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_slot_item, parent, false);

//        mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                listener.onItemClick(v, mViewHolder.getPosition());
//                // row_index= (int) mViewHolder.getItemId();
//
//
//            }
//        });
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
                GetSetGo data = expenseList.get(position);
                 holder.txtSlots.setText(data.getSlot());
//                 holder.txtName.setText(data.getPatientName());

                 holder.txtSlots.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         row_index=position;
                         notifyDataSetChanged();
                         time = holder.txtSlots.getText().toString();
                     }
                 });


        if(row_index==position)
        {
            holder.crdTime.setBackgroundColor(Color.parseColor("#ff6d9b00"));
            holder.txtSlots.setTextColor(Color.parseColor("#ffffff"));
        }
        else
        {
            holder.crdTime.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.txtSlots.setTextColor(Color.parseColor("#000000"));
        }
    }


    @Override
    public int getItemCount() {
        return expenseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtSlots; // , txtName
        CardView crdTime;

        public ViewHolder(View view) {
            super(view);
//            txtName = view.findViewById(R.id.txt_name);
            txtSlots = view.findViewById(R.id.txt_slot);
            crdTime = view.findViewById(R.id.crd_timee);
        }
    }

    public String slot(){
        return time;
    }
}