package com.puretech.ayucare.model;

public class GetSetGo {

    String patientAppointmentId,patientName, patientId, PatientMobile;
    String branchName, city, days, onWeek, branchId;
    String slot;
    String aptBranch, aptDate, aptstatus, aptTime;
    String imgName, imgPath, imgPosiion, imgStatus;
    String productName,productPrice;
    String feedback;


    public String getPatientAppointmentId() {
        return patientAppointmentId;
    }

    public void setPatientAppointmentId(String patientAppointmentId) {
        this.patientAppointmentId = patientAppointmentId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientMobile() {
        return PatientMobile;
    }

    public void setPatientMobile(String patientMobile) {
        PatientMobile = patientMobile;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getOnWeek() {
        return onWeek;
    }

    public void setOnWeek(String onWeek) {
        this.onWeek = onWeek;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }


    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public String getAptBranch() {
        return aptBranch;
    }

    public void setAptBranch(String aptBranch) {
        this.aptBranch = aptBranch;
    }

    public String getAptDate() {
        return aptDate;
    }

    public void setAptDate(String aptDate) {
        this.aptDate = aptDate;
    }

    public String getAptstatus() {
        return aptstatus;
    }

    public void setAptstatus(String aptstatus) {
        this.aptstatus = aptstatus;
    }

    public String getAptTime() {
        return aptTime;
    }

    public void setAptTime(String aptTime) {
        this.aptTime = aptTime;
    }



    //Imag Gallery

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgPosiion() {
        return imgPosiion;
    }

    public void setImgPosiion(String imgPosiion) {
        this.imgPosiion = imgPosiion;
    }

    public String getImgStatus() {
        return imgStatus;
    }

    public void setImgStatus(String imgStatus) {
        this.imgStatus = imgStatus;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
