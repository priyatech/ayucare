package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.puretech.ayucare.R;

public class SplashActivity extends AppCompatActivity {

    Activity activity;
    Thread thread;
    Intent iLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity=this;
        iLogin=new Intent(activity,LoginActivity.class);

        thread=new Thread(){
            public void run(){
                try {
                    Thread.sleep(3000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    startActivity(iLogin);
                    finish();
                }
            }
        };
        thread.start();
    }
}
