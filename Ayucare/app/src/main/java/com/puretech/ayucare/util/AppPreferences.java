package com.puretech.ayucare.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.puretech.ayucare.activity.SplashActivity;
import com.puretech.ayucare.model.UserDTO;


/**
 * Created by rupesh on 14-02-2018.
 */

public class AppPreferences {

    private static AppPreferences instance;
    public static final String NOTIFICATION_BUZZ = "NOTIFICATION_BUZZ";


    public static AppPreferences init(Context context) {
        if (null == instance) {
            instance = new AppPreferences(context);

        }
        return instance;
    }

    private Context context;
    protected SharedPreferences sharedPreferences;

    public AppPreferences(Context context) {
        super();
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * *      Private Methods To Get & Set values in Preferences
     **/

    private int getInteger(String key, int def) {
        int i = sharedPreferences.getInt(key, def);
        return i;
    }

    public String getString(String key, String def) {
        String s = sharedPreferences.getString(key, def);
        return s;
    }

    private boolean getBoolean(String key, boolean def) {
        boolean b = sharedPreferences.getBoolean(key, def);
        return b;
    }

    private double getDouble(String key, double def) {
        double d = Double.parseDouble(sharedPreferences.getString(key, String.valueOf(def)));
        return d;
    }

    private long getLong(String key, long def) {
        return sharedPreferences.getLong(key, def);
    }

    private void setInteger(String key, int val) {
        Editor e = sharedPreferences.edit();
        e.putInt(key, val);
        e.commit();
    }

    public void setString(String key, String val) {
        Editor e = sharedPreferences.edit();
        e.putString(key, val);
        e.commit();
    }

    private void setBoolean(String key, boolean val) {
        Editor e = sharedPreferences.edit();
        e.putBoolean(key, val);
        e.commit();
    }

    private void setDouble(String key, double val) {
        Editor e = sharedPreferences.edit();
        e.putString(key, String.valueOf(val));
        e.commit();
    }

    private void setLong(String key, long val) {
        Editor e = sharedPreferences.edit();
        e.putLong(key, val);
        e.commit();
    }

    /**
     * *      Public Methods To Get & Set Preferences
     **/

    private static final String IS_OTP_SET = "IS_OTP_SET";
    private static final String IS_FIRST_TIME = "IS_FIRST_TIME";
    private static final String IS_FIRST_VIEW = "IS_FIRST_VIEW";
    private static final String IS_FIRST_ON_HOME = "IS_FIRST_ON_HOME";

    public boolean isOtpSet() {
        return getBoolean(IS_OTP_SET, false);
    }

    public void setOtp(boolean isOtp) {
        setBoolean(IS_OTP_SET, isOtp);
    }

    public boolean isFirstTime() {
        return getBoolean(IS_FIRST_TIME, true);
    }

    public void setFirstTime(boolean isFirstTime) {
        setBoolean(IS_FIRST_TIME, isFirstTime);
    }

    public boolean isFirstView() {
        return getBoolean(IS_FIRST_VIEW, true);
    }

    public void setFirstView(boolean isFirstView) {
        setBoolean(IS_FIRST_VIEW, isFirstView);
    }

    public boolean isFirstOnHome() {
        return getBoolean(IS_FIRST_ON_HOME, true);
    }

    public void setFirstOnHome(boolean isFirstOnHome) {
        setBoolean(IS_FIRST_ON_HOME, isFirstOnHome);
    }

    public void setPageId(String PageId) {
        setString(AppConstants.PageId, PageId);
    }

    public String getPageId() {
        return getString(AppConstants.PageId, null);
    }

    public UserDTO getUser() {
        UserDTO user = null;
        Gson gson = new Gson();
        String value = getString(AppConstants.USER, null);
        if (null != value) {
            try {
                user = gson.fromJson(value, UserDTO.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public void setUser(UserDTO user) {
        Gson gson = new Gson();
        String value = null;
        try {
            value = gson.toJson(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setString(AppConstants.USER, value);
    }

    public void setNotificationBuzz(Boolean Notificationbuzz) {
        setBoolean(NOTIFICATION_BUZZ, Notificationbuzz);
    }

    public Boolean getNotificationBuzz() {
        return getBoolean(NOTIFICATION_BUZZ, false);
    }

    public void setIsNotification(Boolean isNotificationRead) {
        setBoolean(AppConstants.NOTIFICATIONREAD, isNotificationRead);
    }

    public Boolean getIsNotification() {
        return getBoolean(AppConstants.NOTIFICATIONREAD, false);
    }

    //Storing Temporiry values from login for OTP page

    public void setActive(String active) {
        setString(AppConstants.ACTIVE, active);
    }

    public String getActive() {
        return getString(AppConstants.ACTIVE, null);
    }


    //Default values after app data clean up
    public void logoutUser(Activity activity) {
        setUser(null);
        setOtp(false);
        setPageId(null);
        setFirstTime(false);
        setFirstOnHome(false);
        setFirstView(false);
        setActive(null);
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

}
