package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.puretech.ayucare.AsynckTask.AsynckCheckLogin;
import com.puretech.ayucare.AsynckTask.AsynkRegister;
import com.puretech.ayucare.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

@SuppressWarnings("ALL")
public class LoginActivity extends AppCompatActivity {

    Activity activity;
    TabLayout tlButton;
    RelativeLayout rl_Signin,rl_Signup;
    Button b_Sign, b_Signup;
    Intent iHome;

    EditText edMobile, edPass;
    EditText et_RName, et_REmail,et_RMobile,et_RPassword;
    String user, pass;
    String name,mobile,password, today;
    AsynckCheckLogin asynckCheckLogin;
    String regMobile;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activity=this;
        tlButton= findViewById(R.id.tv_TabName);
        rl_Signin= findViewById(R.id.rl_Signin);
        rl_Signup= findViewById(R.id.rl_Signup);
        b_Sign= findViewById(R.id.b_Sign);
        b_Signup= findViewById(R.id.b_Signup);

        edMobile= findViewById(R.id.et_Mobile);
        edPass= findViewById(R.id.et_Password);

        et_RName= findViewById(R.id.et_RName);
        et_REmail= findViewById(R.id.et_REmail);
        et_RMobile= findViewById(R.id.et_RMobile);
        et_RPassword= findViewById(R.id.et_RPassword);

        iHome=new Intent(activity,HomeActivity.class);

        tlButton.addTab(tlButton.newTab().setText("Log in"));
        tlButton.addTab((tlButton.newTab().setText("Sign up")));

        tlButton.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        rl_Signin.setVisibility(View.VISIBLE);
                        rl_Signup.setVisibility(View.GONE);
                        break;
                    case 1:
                        rl_Signup.setVisibility(View.VISIBLE);
                        rl_Signin.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        b_Sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user = edMobile.getText().toString();
                pass = edPass.getText().toString();

                if (edMobile.getText().toString().length() == 0) {
                    edMobile.setError("Please Enter userName");
                } else if (edPass.getText().toString().length() == 0) {
                    edPass.setError("Please Enter Password");
                } else
                    asynckCheckLogin =  new AsynckCheckLogin(LoginActivity.this, getApplicationContext());
                    asynckCheckLogin.execute("http://ayucareapp.ayucareclinic.in/Service1.svc/" + LoginActivity.this.getString(R.string.checkLogin) + "/" + user + "/" + pass);

            }
        });


        b_Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = et_RName.getText().toString();
//                email = et_REmail.getText().toString();
                mobile = et_RMobile.getText().toString();
                password = et_RPassword.getText().toString();

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                today = df.format(Calendar.getInstance().getTime());

                if (et_RMobile.getText().toString().length() == 0) {
                    et_RMobile.setError("Please Enter Mobile");
                } else if (et_RPassword.getText().toString().length() == 0) {
                    et_RPassword.setError("Please Enter Password");
                } else if (et_RMobile.getText().toString().length() == 0 && et_RPassword.getText().toString().length() == 0) {
                    et_RPassword.setError("Please Enter Required fields");
                }else
                new AsynkRegister(LoginActivity.this, name, today, mobile,password).execute( LoginActivity.this.getString(R.string.link) + LoginActivity.this.getString(R.string.registration));


            }
        });

    }


//    protected void onRestart() {
//        super.onRestart();
//        regMobile = asynckCheckLogin.patient();
//        Log.d("login mob -->", regMobile);
//        preferences = getApplicationContext().getSharedPreferences("Reg", 0);
//        editor = preferences.edit();
//        String mob = regMobile;
//        editor.putString("mob", mob);
//        editor.apply();
//    }




}
