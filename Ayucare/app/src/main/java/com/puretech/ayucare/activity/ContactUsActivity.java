package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.puretech.ayucare.R;

public class ContactUsActivity extends AppCompatActivity {
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    EditText etName, etPhone, etMessage;
    Button btn_sendMsz;
    String name, phone, message, mobile, email;
    RelativeLayout rlCall, rlMail;
    TextView tvMobile, tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        activity = this;
        tv_Title = findViewById(R.id.tvTitle);
        tv_Title.setText("Contact Us");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        etName = findViewById(R.id.etName);
        etPhone = findViewById(R.id.etPhone);
        etMessage = findViewById(R.id.etMessage);
        btn_sendMsz = findViewById(R.id.btn_sendMsz);
        rlCall = findViewById(R.id.rlCall);
        rlMail = findViewById(R.id.rlMail);
        tvMobile = findViewById(R.id.tvMobile);
        tvEmail = findViewById(R.id.tvEmail);

        btn_sendMsz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = etName.getText().toString().trim();
                phone = etPhone.getText().toString().trim();
                message = etMessage.getText().toString().trim();
            }
        });

        rlCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile = tvMobile.getText().toString();
                makeCall();
            }
        });

        rlMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = tvEmail.getText().toString();
                sendEmail();
            }
        });
    }

    // send Email
    public void sendEmail(){
        String[] recipients={email};
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Contact made through android app");
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        }else{
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gm")));
        }
    }

    // make phone call
    public void makeCall(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobile));
        activity.startActivity(intent);
    }

}
