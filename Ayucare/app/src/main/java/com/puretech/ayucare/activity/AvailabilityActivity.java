package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;

public class AvailabilityActivity extends AppCompatActivity {
//    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    Button btn_Appointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability);
//        activity=this;
        tv_Title= findViewById(R.id.tvTitle);
        tv_Title.setText("Availability");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_Appointment = findViewById(R.id.btn_Appointment);
        btn_Appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(AvailabilityActivity.this, Branch.class);
                startActivity(i);
            }
        });

    }
}
