package com.puretech.ayucare.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.App;
import com.puretech.ayucare.AsynckTask.AsynkGetGallery;
import com.puretech.ayucare.R;
import com.puretech.ayucare.adapter.AdapterImage;
import com.puretech.ayucare.model.ItemCategory;
import com.puretech.ayucare.util.CommonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class GalleryActivity extends AppCompatActivity {
    App app;
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    Bundle bundle;
//    Intent intent;
//    View rootView;
//    Dialog dialog;
//    GridView gvImage;
//    AdapterImage adapterImage;
//    private List<ItemCategory> categoryList = new ArrayList<>();
//    ViewGroup.LayoutParams list;
    RecyclerView recycler_gallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        activity = this;
        app = (App) getApplication();

        tv_Title= findViewById(R.id.tvTitle);
        tv_Title.setText("Gallery");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recycler_gallery = findViewById(R.id.recycler_my_gallery);

        new AsynkGetGallery(getApplicationContext(), GalleryActivity.this, recycler_gallery)
                .execute(GalleryActivity.this.getString(R.string.link) + GalleryActivity.this.getString(R.string.getGallary));


//        list = (ViewGroup.LayoutParams) gvImage.getLayoutParams();

        checkConnection();

    }

    public void checkConnection(){
        if (CommonUtil.isNetworkAvailable(activity)){
            //getImageGallery();
        }else{
            app.showSnackBar(activity, "Please check your network connection.");
        }
    }

//    public void getImageGallery(){
//        dialog.show();
//        app.getApiService().getImages().enqueue(new Callback<Map<String, Object>>() {
//            @Override
//            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
//                if (response.body() != null) {
//                    categoryList.clear();
//                    app.getLogger().error("success");
//                    String status = "";
//                    String message = "";
//                    JSONObject jobj = null;
//                    try{
//                        jobj = new JSONObject(response.body());
//                        status = jobj.getString("success");
//                        message = jobj.getString("message");
//                        JSONArray school = jobj.getJSONArray("result");
//
//                        if (status.equals("1")){
//                            for (int i = 0; i < school.length(); i++) {
//                                JSONObject c = school.getJSONObject(i);
//
//                                String id = c.getString("id");
//                                id = id.substring(0, id.length() - 2);
//                                String image = c.getString("image");
//                                String createdAt = c.getString("created_at");
//                                String updatedAt = c.getString("updated_at");
//
//                                ItemCategory itemCategory = new ItemCategory();
//                                itemCategory.setId(id);
//                                itemCategory.setImage(image);
//                                itemCategory.setCreatedAt(createdAt);
//                                itemCategory.setUpdatedAt(updatedAt);
//                                categoryList.add(itemCategory);
//                            }
//                        }else{
//                            app.showSnackBar(activity, "No records found.");
//                        }
//
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//
//                } else {
//                    int a = response.code();
//                    String b = response.message();
//                    if (a == 401){
//                        //logout();
//                        app.showSnackBar(activity, "Token Expired. Please Login again");
//                    }else{
//                        app.showSnackBar(activity, "No record found");
//                    }
//
//                }
//                dialog.cancel();
//                setAdapter();
//
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                app.getLogger().error("failure");
//                //dialog.dismiss();
//            }
//        });
//    }
//
//    public void setAdapter(){
//        if (categoryList == null)
//            return;
//
//        //i = (categoryList.size()*250);
//        adapterImage = new AdapterImage(categoryList, activity);
//        gvImage.setAdapter(adapterImage);
//        //list.height = i;
//        //gvImage.setLayoutParams(list);
//    }

}
