package com.puretech.ayucare.activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.AsynckTask.AsynkGetProducts;
import com.puretech.ayucare.R;

public class ProductActivity extends AppCompatActivity {
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        activity=this;
        tv_Title=(TextView)findViewById(R.id.tvTitle);
        tv_Title.setText("Products");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.recy_products);

        new AsynkGetProducts(getApplicationContext(), ProductActivity.this, recyclerView).execute(ProductActivity.this.getString(R.string.link) + ProductActivity.this.getString(R.string.getProducts));



    }
}
