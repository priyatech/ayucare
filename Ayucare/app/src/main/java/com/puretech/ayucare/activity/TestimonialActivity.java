package com.puretech.ayucare.activity;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.puretech.ayucare.App;
import com.puretech.ayucare.AsynckTask.AsynkTestimonals;
import com.puretech.ayucare.R;
import com.puretech.ayucare.adapter.AdapterCategory;
import com.puretech.ayucare.adapter.AdapterTestimonials;
import com.puretech.ayucare.model.ItemCategory;
import com.puretech.ayucare.model.ItemTestimonials;
import com.puretech.ayucare.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

public class TestimonialActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    App app;
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
//    ListView lvTestimonials;
    RecyclerView recyclerView_testimonals;
//    AdapterTestimonials adapterTestimonials;
//    List<ItemTestimonials> testimonialsList = new ArrayList<>();
//    long totalSize = 0;
//    Boolean isPermitted = true;
//    String message, alert;
    SwipeRefreshLayout swipeRefreshLayout;
//    String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimonial);
        app = (App) getApplication();
        activity=this;
        tv_Title=(TextView)findViewById(R.id.tvTitle);
        tv_Title.setText("Testimonials");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView_testimonals = findViewById(R.id.lvTestimonials);

        new AsynkTestimonals(getApplicationContext(), TestimonialActivity.this, recyclerView_testimonals).execute(TestimonialActivity.this.getString(R.string.link) + TestimonialActivity.this.getString(R.string.getTestimonals));

        swipeRefreshLayout =  findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        new AsynkTestimonals(getApplicationContext(), TestimonialActivity.this, recyclerView_testimonals).execute(TestimonialActivity.this.getString(R.string.link) + TestimonialActivity.this.getString(R.string.getTestimonals));

                                        //checkConnection();
                                    }
                                }
        );

        checkConnection();

    }

    @Override
    public void onRefresh() {
        checkConnection();
    }

    public void checkConnection() {
        if (CommonUtil.isNetworkAvailable(activity)) {
            swipeRefreshLayout.setRefreshing(false);
            //checkRunTimePermissionPic();
        } else {
            app.showSnackBar(activity, "Please check network connectivity");
            //setAdapter();
        }
    }

}
