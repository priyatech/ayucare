package com.puretech.ayucare.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.App;
import com.puretech.ayucare.AsynckTask.AsynckBookApptment;
import com.puretech.ayucare.AsynckTask.AsynckCheckLogin;
import com.puretech.ayucare.AsynckTask.AsynkGetSlots;
import com.puretech.ayucare.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;

import static com.puretech.ayucare.App.TAG;

@SuppressWarnings("All")

public class BookAppointmentActivity extends AppCompatActivity {
    App app;
    Activity activity;
    TextView tv_Title;
    ImageView iv_Back;
    EditText edtName, edtNumber;
    private HorizontalCalendar horizontalCalendar;
    View view;
//    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9;
    Button btnConfirm;
    String apptDate, apptTime, currDate, currTime, slot, branchId;
//    int i1=0, i2=0, i3=0,i4=0, i5=0, i6=0,i7=0, i8=0, i9=0;
    Bundle b;
    RecyclerView recyclerSlot;
    AsynkGetSlots asynkGetSlots;
    AsynckCheckLogin asynckCheckLogin;
    private SharedPreferences sharedPreferences;
    String mob;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);
        activity=this;
        app = (App) getApplication();

        tv_Title=(TextView)findViewById(R.id.tvTitle);
        tv_Title.setText("Book an Appointment");

        iv_Back = findViewById(R.id.ivBack);
        edtName = findViewById(R.id.etName);
//        edtNumber = findViewById(R.id.etPhone);
        btnConfirm = findViewById(R.id.btnConfirm);
        recyclerSlot = findViewById(R.id.recycler_slots);

        b = getIntent().getExtras();
        branchId = b.getString("branchId");

        sharedPreferences = getSharedPreferences("Reg", Context.MODE_PRIVATE);
        mob = sharedPreferences.getString("mob", "0");


        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat Dateformat = new SimpleDateFormat("yyyy-MM-dd");
        currDate = Dateformat.format(calendar.getTime());
        Log.d("currDate:" , currDate);

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm");
        currTime = mdformat.format(calendar.getTime());

        asynkGetSlots = new AsynkGetSlots(getApplicationContext(), BookAppointmentActivity.this, recyclerSlot);
        asynkGetSlots.execute(BookAppointmentActivity.this.getString(R.string.link) + BookAppointmentActivity.this.getString(R.string.getData));
//        asynkGetSlots.execute(BookAppointmentActivity.this.getString(R.string.link) + BookAppointmentActivity.this.getString(R.string.getData)+"/"+currDate+"/"+branchId);


        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);

        btnConfirm = findViewById(R.id.btnConfirm);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, 0);

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(today.getTime())
                .endDate(endDate.getTime())
                .defaultSelectedDate(today.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .textSize(12f, 14f, 12f)
                .showDayName(true)
                .showMonthName(true)
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                String month = null;
                int hr = 0;
                int d = date.getDate();
                int m = date.getMonth()+1;
                int y = date.getYear()+1900;
                hr = date.getHours();
                int min = date.getMinutes();

                String day = String.valueOf(d);
                if (m<10){
                    month = "0"+String.valueOf(m);
                }else {
                    month = String.valueOf(m);
                }
                String year = String.valueOf(y);
                String time = String.valueOf(hr)+":"+String.valueOf(min);
                currTime = time;
                apptDate = year+"-"+month+"-"+day;

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateInstalled = null;

                try {
                    dateInstalled = sdf.parse(apptDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String dateInstall = sdf.format(dateInstalled);
//                Log.i("dateInstall-->",dateInstall);


//                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//                Date newdate = new Date();
//                currDate = dateFormat.format(newdate);

//                if (apptDate.equals(currDate)){
//                    if (hr>9 && hr<10){
//                        tv1.setEnabled(false);
//                    }else if (hr>10 && hr<11){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                    }else if (hr>11 && hr<12){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                        tv3.setEnabled(false);
//                    }else if (hr>12 && hr<14){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                        tv3.setEnabled(false);
//                        tv4.setEnabled(false);
//                    }else if (hr>14 && hr<15){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                        tv3.setEnabled(false);
//                        tv4.setEnabled(false);
//                        tv5.setEnabled(false);
//                    }else if (hr>15 && hr<16){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                        tv3.setEnabled(false);
//                        tv4.setEnabled(false);
//                        tv5.setEnabled(false);
//                        tv6.setEnabled(false);
//                    }else if (hr>16 && hr<17){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                        tv3.setEnabled(false);
//                        tv4.setEnabled(false);
//                        tv5.setEnabled(false);
//                        tv6.setEnabled(false);
//                        tv7.setEnabled(false);
//                    }else if (hr>=17){
//                        tv1.setEnabled(false);
//                        tv2.setEnabled(false);
//                        tv3.setEnabled(false);
//                        tv4.setEnabled(false);
//                        tv5.setEnabled(false);
//                        tv6.setEnabled(false);
//                        tv7.setEnabled(false);
//                        tv8.setEnabled(false);
//                        tv9.setEnabled(false);
//                        btnConfirm.setEnabled(false);
//                    }else{
//                        tv1.setEnabled(true);
//                        tv2.setEnabled(true);
//                        tv3.setEnabled(true);
//                        tv4.setEnabled(true);
//                        tv5.setEnabled(true);
//                        tv6.setEnabled(true);
//                        tv7.setEnabled(true);
//                        tv8.setEnabled(true);
//                        tv9.setEnabled(true);
//                    }
//                }else{
//                    tv1.setEnabled(true);
//                    tv2.setEnabled(true);
//                    tv3.setEnabled(true);
//                    tv4.setEnabled(true);
//                    tv5.setEnabled(true);
//                    tv6.setEnabled(true);
//                    tv7.setEnabled(true);
//                    tv8.setEnabled(true);
//                    tv9.setEnabled(true);
//                }

            }
            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView,
                                         int dx, int dy) {
            }
            @Override
            public boolean onDateLongClicked(Date date, int position) {
                return true;
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String aptTime = asynkGetSlots.mySlot();
//                Log.d("aptTime -->", aptTime);
//                Log.d("mob -->", mob);
                // apptdtea, appTime, Name, mobile,slot,Branch
                String name, mobile;
                name = edtName.getText().toString();

                if (edtName.getText().toString().length() == 0) {
                    edtName.setError("Please Enter Name");
                } else
                new AsynckBookApptment(BookAppointmentActivity.this, apptDate,aptTime,name,mob,branchId).execute(BookAppointmentActivity.this.getString(R.string.link) + BookAppointmentActivity.this.getString(R.string.bookAppointment));
            }
        });

    }

}
