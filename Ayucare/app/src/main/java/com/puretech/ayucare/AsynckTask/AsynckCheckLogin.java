package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.puretech.ayucare.R;
import com.puretech.ayucare.activity.HomeActivity;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class AsynckCheckLogin extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    Context context;
    String userid;
    ProgressDialog pDialog = null;
    GetSetGo getSetGo;
    private List<GetSetGo> dataList = new ArrayList<>();
    String patientName;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    // For checking login authentication
    public AsynckCheckLogin(Activity act , Context context) {
        this.activity = act;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();

    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    patientName = object.getString("PatientName");
                    final String patientId = object.getString("PatientId");
                    final String patientMobile = object.getString("Mobile");

                    getSetGo = new GetSetGo();

                    getSetGo.setPatientName(patientName);
                    getSetGo.setPatientId(patientId);
                    getSetGo.setPatientMobile(patientMobile);

                    userid = getSetGo.getPatientMobile();
                    System.out.println(userid);

                    dataList.add(getSetGo);

                }
                // to send data to dashboard class
                if (patientName!= null) {
                    preferences = activity.getPreferences(MODE_PRIVATE);
                    Toast.makeText(activity, "success", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(activity, HomeActivity.class);
//                i.putExtra("patientName", userid);
                    Log.d("login mob -->", userid);
                    preferences = context.getSharedPreferences("Reg", 0);
                    editor = preferences.edit();
                    String mob = userid;
                    editor.putString("mob", mob);
                    editor.apply();
                    activity.startActivity(i);
                }else{
                    Toast.makeText(activity, "Wrong credentials", Toast.LENGTH_SHORT).show();
                }

            }

            catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get any data.", Toast.LENGTH_LONG).show();
        }
    }

}