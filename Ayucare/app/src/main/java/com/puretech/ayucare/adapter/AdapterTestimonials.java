package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.model.ItemTestimonials;
import com.puretech.ayucare.util.ImageUtil;

import java.util.List;

/**
 * Created by Rupesh on 18-05-2018.
 */

public class AdapterTestimonials extends BaseAdapter {
    private List<ItemTestimonials> itemTestimonials;
    private Activity activity;
    private LayoutInflater layoutInflater;

    public AdapterTestimonials(List<ItemTestimonials> itemTestimonials, Activity activity) {
        this.itemTestimonials = itemTestimonials;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemTestimonials.size();
    }

    @Override
    public ItemTestimonials getItem(int position) {
        return itemTestimonials.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AdapterTestimonials.ViewHolder viewHolder;
        final ItemTestimonials itemTestimonials = getItem(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_testimonials, parent, false); //item_friends
            viewHolder = new AdapterTestimonials.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdapterTestimonials.ViewHolder) convertView.getTag();
        }

        //String str = itemTestimonials.getImage();
        ImageUtil.displayImage(viewHolder.ivCreator, itemTestimonials.getImage(), null);
        viewHolder.tvDescription.setText(itemTestimonials.getDescription());
        viewHolder.tvCreator.setText(itemTestimonials.getCreatedBy());


        return convertView;
    }

    private class ViewHolder {

        private ImageView ivCreator;
        private TextView tvDescription, tvCreator;

        private ViewHolder(View view) {
            ivCreator = (ImageView) view.findViewById(R.id.ivCreator);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            tvCreator = (TextView) view.findViewById(R.id.tvCreator);
        }
    }

}
