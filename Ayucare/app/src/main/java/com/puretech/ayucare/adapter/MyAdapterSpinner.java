package com.puretech.ayucare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.puretech.ayucare.R;
import com.puretech.ayucare.activity.BookAppointmentActivity;
import com.puretech.ayucare.model.GetSetGo;

import java.util.List;

public class MyAdapterSpinner extends ArrayAdapter {

    Activity activity;
    List<GetSetGo> DETAILS ;

    public MyAdapterSpinner(@NonNull Context context, int resource, Activity activity, List<GetSetGo> DETAILS) {
        super(context, resource);
        this.activity = activity;
        this.DETAILS = DETAILS;
    }



    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.branch_spinner, parent, false);

        //Set Custom View
        TextView txtBranch, txtDays;
        Button btnBook;
        txtBranch   = view.findViewById(R.id.txt_branch);
        txtDays = view.findViewById(R.id.txt_days);
        btnBook = view.findViewById(R.id.btnBook);

        txtBranch.setText(DETAILS.get(position).getBranchName());
        txtDays.setText(DETAILS.get(position).getDays());

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContext().startActivity(new Intent(getContext(),BookAppointmentActivity.class));
            }
        });

        return view;
    }

    @Override
    public View getDropDownView(int position,View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}