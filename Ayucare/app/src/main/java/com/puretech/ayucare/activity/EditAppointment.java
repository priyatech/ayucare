package com.puretech.ayucare.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.puretech.ayucare.AsynckTask.AsynkCancelApt;
import com.puretech.ayucare.AsynckTask.AsynkGetBranch;
import com.puretech.ayucare.AsynckTask.AsynkGetSlots;
import com.puretech.ayucare.AsynckTask.AsynkUpdateApt;
import com.puretech.ayucare.AsynckTask.GetBranch;
import com.puretech.ayucare.R;
import com.puretech.ayucare.model.GetSetGo;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.icu.text.RelativeDateTimeFormatter.AbsoluteUnit.DAY;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

@SuppressWarnings("deprecation")
public class EditAppointment extends AppCompatActivity {
    TextView tv_Title, txtDate,txtId;
    EditText edtPName;
    ImageView iv_Back;
    View view;
    RecyclerView rcTime, rcBranch;
    Spinner spnBranch;
    Button btnUpdate;
    Calendar myCalendar;
    int YEAR,MONTH,DAY;
    static final int DDILOG_ID = 0 ;
    Bundle b;
    AsynkGetSlots asynkGetSlots;
    GetBranch asynkGetBranch;
    private List<GetSetGo> dataList = new ArrayList<>();
    String getCategory, branchIdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_appointment);

        tv_Title = findViewById(R.id.tvTitle);
        tv_Title.setText("Update Appointment");
        iv_Back = findViewById(R.id.ivBack);
        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        view = findViewById(R.id.view);

        edtPName  = findViewById(R.id.et_PName);
        txtId     = findViewById(R.id.txt_patient_id);
        txtDate   = findViewById(R.id.et_apt_date);
        rcTime   = findViewById(R.id.rc_time);
        spnBranch   = findViewById(R.id.spn_branch);
        btnUpdate = findViewById(R.id.btn_update);


        b = getIntent().getExtras();
        if(b != null) {
            txtId.setText(b.getString("pId"));
            edtPName.setText(b.getString("pName"));
        }


        asynkGetSlots = new AsynkGetSlots(getApplicationContext(), EditAppointment.this, rcTime);
        asynkGetSlots.execute(EditAppointment.this.getString(R.string.link) + EditAppointment.this.getString(R.string.getData));


       asynkGetBranch = new GetBranch(getApplicationContext(),EditAppointment.this,spnBranch);
        asynkGetBranch.execute(EditAppointment.this.getString(R.string.link) + EditAppointment.this.getString(R.string.getBranch));

        myCalendar = Calendar.getInstance();

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(DDILOG_ID);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id, date, branch,name;

                String time = asynkGetSlots.mySlot();

                dataList = asynkGetBranch.getBranch();

                   getCategory = spnBranch.getSelectedItem().toString().trim();

                    for (int i = 0; i < dataList.size(); i++) {
                        if (dataList.get(i).getBranchName().equals(getCategory)) {

                            GetSetGo details = new GetSetGo();
                            details.setBranchId(dataList.get(i).getBranchId());
                            branchIdd = details.getBranchId();
                            Log.d("branchIdd--->>", branchIdd);
                        }
                }

                 id = txtId.getText().toString();
                 date = txtDate.getText().toString();
                 name = edtPName.getText().toString();
                new AsynkUpdateApt(EditAppointment.this,date,time , name, id, branchIdd).execute(EditAppointment.this.getString(R.string.link) + EditAppointment.this.getString(R.string.UpdateApt));
            }
        });

    }

    protected Dialog onCreateDialog(int id){
        if (id == DDILOG_ID)
        {

            Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(EditAppointment.this,R.style.DialogTheme,datepickerlist,mYear,mMonth,mDay);
//            dialog.show();

//            DatePickerDialog dialog = new DatePickerDialog(EditAppointment.this,R.style.DialogTheme,datepickerlist,YEAR,MONTH,DAY);
//            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 1);
            Date newDate = c.getTime();
//            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  dialog;

        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener datepickerlist = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            YEAR = year;
            MONTH= month+1;
            DAY = dayOfMonth;
            txtDate.setText(YEAR+"-"+MONTH+"-"+DAY);
        }
    };


}
