package com.puretech.ayucare.AsynckTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.puretech.ayucare.App;
import com.puretech.ayucare.adapter.ProductAdapter;
import com.puretech.ayucare.adapter.TestimonalAdapter;
import com.puretech.ayucare.model.GetSetGo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AsynkTestimonals extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    GetSetGo dataDTO;
    ProgressDialog pDialog = null;
    App app;
    private ArrayList<GetSetGo> dataList = new ArrayList<>();

    TestimonalAdapter testimonalAdapter;
    Context context;
    RecyclerView recyclerViewBranch;
    String slot;
    // For checking login authentication
    public AsynkTestimonals( Context context , Activity act,RecyclerView recyclerViewBranch) {
        this.context = context;
        this.activity = act;
        this.recyclerViewBranch = recyclerViewBranch;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }else if (result == 401){
                //logout();
                Toast.makeText(activity, "Bad Request.", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(activity, "No data found.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String pName, imgPath, feedback;
                    pName = object.getString("PName");
                    imgPath = object.getString("ImgPath");
                    feedback = object.getString("Feedback");

                    dataDTO = new GetSetGo();
                    dataDTO.setPatientName(pName);
                    dataDTO.setImgPath(imgPath);
                    dataDTO.setFeedback(feedback);
                    dataList.add(dataDTO);

                }
                testimonalAdapter = new TestimonalAdapter(dataList,activity);

                // this for grid view item
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                recyclerViewBranch.setLayoutManager(mLayoutManager);
                recyclerViewBranch.setItemAnimator(new DefaultItemAnimator());
                recyclerViewBranch.setAdapter(testimonalAdapter);




            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get data.", Toast.LENGTH_LONG).show();
        }
    }

}